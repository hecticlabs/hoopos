# hoopOS
The secure & modern mobile operating system for the Raspberry Pi Pico.

## Building & running
```
$ ./task default
```

## Progress
- `[x]` Boot firmware
    - `[x]` BIOS functions
    - `[x]` Panic handler
- `[ ]` Use [fulake](https://gitlab.com/hecticlabs/fulake) instead of Pico SDK
- `[-]` Mach-O loading
    - `[x]` Mach-O allocation
    - `[x]` Mach-O relocation
    - `[ ]` Patched linker
    - `[ ]` Mach-O dynamic linking
- `[ ]` *hold* dynamic linker
    - `[ ]` Static functions
    - `[ ]` Dynamic libraries
- `[-]` Syscalls
    - `[x]` SVCall
    - `[ ]` Parameters
    - `[ ]` Return values
- `[ ]` VVM
    - `[ ]` kalloc
    - `[ ]` VM alloc
    - `[ ]` VM slices & stacks
    - `[ ]` VM MPU
- `[ ]` Preemptive task scheduling
    - `[ ]` Trigger (alarm/timer/SysTick/PendSV)
    - `[ ]` Stack switching
    - `[ ]` Page and slice switching
    - `[ ]` Scheduler
- `[ ]` Secure boot
    - `[ ]` Kernel loading
    - `[ ]` Very fast SHA-256
    - `[ ]` Fast Ed25519
    - `[ ]` Kernel checksum & integrity check
- `[ ]` Custom USB serial library
- `[ ]` Recovery Mode
    - `[ ]` Serial protocol
    - `[ ]` Safety checks
    - `[ ]` Loading ramdisks
    - `[ ]` Kernel restore
    - `[ ]` Filesystem restore
    - `[ ]` Filesystem update
- `[ ]` Filesystem
    - `[ ]` VFS
    - `[ ]` RAMFS
    - `[ ]` HOFS
    - `[ ]` ext2
- `[ ]` POSIX
    - `[ ]` Processes
    - `[ ]` File operations
    - `[ ]` Generic functions
    - `[ ]` Sockets
- `[ ]` Drivers
    - `[ ]` SD Card
    - `[ ]` ILI9341
    - `[ ]` Networking
    - `[ ]` Bluetooth

**Credits**
- [Ainara](https://ainara.dev) Lead developer
- [Mineek](https://github.com/mineek) Help with Mach-O loader
- [Lina](https://github.com/nullium21) Habanero format
- [kney](about:blank) Mach-O loader, Dynamic libraries, Filesystem
- [Nick Chan](https://github.com/asdfugil) Dynamic linker, Compiler/linker debugging
- [HAHALOSAH](https://github.com/HAHALOSAH) Debugging
- [plx](https://twitter.com/plzdonthaxme) Compiler/linker debugging

Special thanks to [stx4](https://github.com/stx3plus1), [Mineek](https://github.com/mineek) and [Ellie](https://github.com/ElliesSurviving) for emotional support.
