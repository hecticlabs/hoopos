#include "hofs.h"
#include <strings.h>

static inline uint64_t _rotl(uint64_t value, uint8_t shift) {
  return (value << shift) | (value >> (64 - shift));
}

static inline uint64_t _rotr(uint64_t value, uint8_t shift) {
  return (value >> shift) | (value << (64 - shift));
}

uint64_t hoop_hofs_enc_hash(uint64_t base, uint8_t *src, size_t len) {
  uint64_t p = base;
  for (int i = 0; i < 64; i++) {
    for (int j = 0; j < len; j++) {
      uint64_t q = _rotl(src[j], i) ^ _rotr(base, 64 - i);
      if (q == 0)
        q = 7;
      p = ((p ^ q) >> 5) * 7;
    }
  }
  return p;
}

uint64_t hoop_hofs_enc_merge(uint64_t a, uint64_t b) {
  uint64_t p = a;

  for (int i = 0; i < 64; i++) {
    for (int j = 0; j < 64; j++) {
      uint64_t q = _rotl(a, i) ^ _rotr(b, j);
      if (q == 0)
        q = 3;
      p = ((p ^ q) >> 3) * 5;
    }
  }
  return p;
}

uint8_t *hoop_hofs_enc_encrypt(uint64_t key, uint32_t block, uint8_t *buf, size_t len) {
  key = _rotl(key, block % 64) ^ block;

  for (size_t i = 0; i < len; i++) {
    buf[i] = buf[i] ^ _rotr(key, i * 4);
    key = ((key >> 7) * 13) ^ key;
  }
  return buf;
}

void hoop_hofs_block_read(uint8_t blockn, uint8_t *buf);
void hoop_hofs_block_write(uint8_t blockn, uint8_t *buf);

hoop_hofs_inode *hoop_hofs_get_inode(hoop_hofs_superblock *sb, uint32_t inode, uint8_t *buf) {
  uint32_t offset = inode * sizeof(hoop_hofs_inode) + sizeof(hoop_hofs_superblock);
  uint32_t blockn = offset / HOOP_HOFS_BLOCK_SIZE;
  hoop_hofs_block_read(blockn, buf);
  uint32_t byteoffset = offset - blockn * HOOP_HOFS_BLOCK_SIZE;
  return (hoop_hofs_inode*) (&buf[byteoffset]);
}

int hoop_hofs_read_inode(hoop_hofs_superblock *sb, hoop_hofs_inode *inode, uint32_t offset, uint8_t *dst, uint32_t len, uint8_t *buf) {
  if (offset >= inode->size) {
    return -1; // tried to read out of bounds
  }

  uint32_t block_base = sb->ninodes * sizeof(hoop_hofs_inode) + sizeof(hoop_hofs_superblock);

  while (len > 0) {
    uint32_t entry = offset / HOOP_HOFS_BLOCK_SIZE;
    if (entry < 7) {
      uint32_t direct = inode->direct[entry] - 1;
      if (direct < 0) {
        return len; // tried to read out of bounds
      }

      uint32_t blockn = block_base + direct;
      uint32_t startoffset = offset - entry * HOOP_HOFS_BLOCK_SIZE;

      hoop_hofs_block_read(blockn, buf);
      memcpy(dst, &buf[startoffset], HOOP_HOFS_BLOCK_SIZE - startoffset);
      len -= HOOP_HOFS_BLOCK_SIZE - startoffset;
      offset += HOOP_HOFS_BLOCK_SIZE - startoffset;
    } else {
      if (inode->indirect < 1) {
        return len; // tried to read out of bounds
      }
      if (entry - 7 >= HOOP_HOFS_BLOCK_SIZE / sizeof(uint32_t)) {
        return len;  // tried to read out of bounds
      }

      hoop_hofs_block_read(block_base + inode->indirect - 1, buf);
      uint32_t direct = ((uint32_t*) buf)[entry - 7];

      uint32_t blockn = block_base + direct;
      uint32_t startoffset = offset - entry * HOOP_HOFS_BLOCK_SIZE;

      hoop_hofs_block_read(blockn, buf);
      memcpy(dst, &buf[startoffset], HOOP_HOFS_BLOCK_SIZE - startoffset);
      len -= HOOP_HOFS_BLOCK_SIZE - startoffset;
      offset += HOOP_HOFS_BLOCK_SIZE - startoffset;
    }
  }

  return 0;
}

uint32_t hoop_hofs_alloc_block(hoop_hofs_superblock *sb, uint8_t *buf) {
  for (int i = 0; i < 16; i++) {
    for (int j = 0; j < 32; j++) {
      if (i * 32 + j >= sb->nblocks) {
        return 0;
      }

      if ((sb->freeblocks[i] & (1 << j)) == 0) {
        sb->freeblocks[i] |= 1 << j;

        hoop_hofs_block_read(0, buf);
        memcpy(buf, sb, sizeof(hoop_hofs_superblock));
        hoop_hofs_block_write(0, buf);

        return i * 32 + j + 1;
      }
    }
  }
  return 0;
}

int hoop_hofs_write_inode(hoop_hofs_superblock *sb, hoop_hofs_inode *inode, uint32_t offset, uint8_t *src, uint32_t len, uint8_t *buf) {
  // TODO the whole fucking function lmao
}
